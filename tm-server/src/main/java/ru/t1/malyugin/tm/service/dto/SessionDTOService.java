package ru.t1.malyugin.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.api.service.dto.ISessionDTOService;
import ru.t1.malyugin.tm.dto.model.SessionDTO;
import ru.t1.malyugin.tm.repository.dto.SessionDTORepository;

import javax.persistence.EntityManager;

public final class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO> implements ISessionDTOService {

    public SessionDTOService(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    protected ISessionDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionDTORepository(entityManager);
    }

}