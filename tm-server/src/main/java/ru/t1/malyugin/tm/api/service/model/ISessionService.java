package ru.t1.malyugin.tm.api.service.model;

import ru.t1.malyugin.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

}