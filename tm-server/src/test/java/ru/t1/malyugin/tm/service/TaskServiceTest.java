package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.entity.TaskNotFoundException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.NameEmptyException;
import ru.t1.malyugin.tm.exception.field.UserIdEmptyException;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.model.User;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static ru.t1.malyugin.tm.TestData.*;

@Category(UnitCategory.class)
public class TaskServiceTest {

    @NotNull
    private static User user1 = new User();

    @NotNull
    private static User user2 = new User();

    @NotNull
    private Project project = new Project();

    @BeforeClass
    public static void initUser() {
        user1 = USER_SERVICE.create(FIRST_USUAL_USER_LOGIN, FIRST_USUAL_USER_PASS, null, Role.USUAL);
        user2 = USER_SERVICE.create(SECOND_USUAL_USER_LOGIN, SECOND_USUAL_USER_PASS, null, Role.USUAL);
    }

    @AfterClass
    public static void clearUser() {
        USER_SERVICE.removeById(user1.getId());
        USER_SERVICE.removeById(user2.getId());
    }

    @Before
    public void initTest() {
        project = new Project("TST_P", "TST_D");
        project.setUser(user1);
        PROJECT_SERVICE.add(project);

        for (int i = 1; i <= NUMBER_OF_TASKS; i++) {
            @NotNull final Task task = new Task("P" + i, "D" + i);
            if (i <= NUMBER_OF_TASKS / 2) task.setUser(user1);
            else task.setUser(user2);
            if (i <= 3) task.setProject(project);
            TASK_SERVICE.add(task);
            TASK_LIST.add(task);
        }
    }

    @After
    public void clearData() {
        TASK_SERVICE.clear();
        PROJECT_SERVICE.clear();
        TASK_LIST.clear();
        PROJECT_LIST.clear();
    }

    @Test
    public void testClear() {
        final int expected = 0;
        TASK_SERVICE.clear();
        Assert.assertEquals(expected, TASK_SERVICE.getSize());
    }

    @Test
    public void testClearForUser() {
        TASK_SERVICE.clear(UNKNOWN_ID);
        Assert.assertEquals(NUMBER_OF_TASKS, TASK_SERVICE.getSize());
        TASK_SERVICE.clear(user1.getId());
        final int expected = 0;
        Assert.assertEquals(expected, TASK_SERVICE.getSize(user1.getId()));
        Assert.assertNotEquals(expected, TASK_SERVICE.getSize(user2.getId()));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(TASK_LIST.size(), TASK_SERVICE.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        final long actualProjectList = TASK_SERVICE.getSize(user1.getId());
        Assert.assertEquals(actualProjectList, TASK_SERVICE.getSize(user1.getId()));
        Assert.assertEquals(0, TASK_SERVICE.getSize(UNKNOWN_ID));
    }

    @Test
    public void testGetSizeForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.getSize(null));
    }


    @Test
    public void testCrateTask() {
        final int expectedNumberOfEntries = NUMBER_OF_TASKS + 2;
        @NotNull final Task task1 = TASK_SERVICE.create(user1.getId(), "NAME1", "DESCRIPTION");
        @NotNull final Task task2 = TASK_SERVICE.create(user2.getId(), "NAME2", null);

        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
        Assert.assertEquals("NAME1", task1.getName());
        Assert.assertEquals("DESCRIPTION", task1.getDescription());
        Assert.assertEquals(user1.getId(), task1.getUser().getId());

        Assert.assertEquals("NAME2", task2.getName());
        Assert.assertEquals("", task2.getDescription());
        Assert.assertEquals(user2.getId(), task2.getUser().getId());
    }

    @Test
    public void testCrateTaskNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create(null, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create(null, UNKNOWN_ID, null));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(UNKNOWN_ID, null, UNKNOWN_ID));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(UNKNOWN_ID, null, null));
    }

    @Test
    public void testUpdateById() {
        @NotNull final Task task = TASK_SERVICE.findAll(user1.getId()).get(0);
        @NotNull final String test = "TEST";
        TASK_SERVICE.updateById(user1.getId(), task.getId(), test, null);
        TASK_SERVICE.updateById(user1.getId(), task.getId(), test, test);

        @Nullable final Task actualTask = TASK_SERVICE.findOneById(user1.getId(), task.getId());
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(test, actualTask.getName());
        Assert.assertEquals(test, actualTask.getDescription());
    }

    @Test
    public void testUpdateByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.updateById(null, UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.updateById(UNKNOWN_ID, null, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(TaskNotFoundException.class, () -> TASK_SERVICE.updateById(UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.updateById(UNKNOWN_ID, UNKNOWN_ID, null, UNKNOWN_ID));
    }

    @Test
    public void testChangeTaskStatusById() {
        @NotNull final Task task = TASK_SERVICE.findAll(user1.getId()).get(0);
        TASK_SERVICE.changeStatusById(user1.getId(), task.getId(), Status.COMPLETED);
        @Nullable Task actualTask = TASK_SERVICE.findOneById(user1.getId(), task.getId());
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(Status.COMPLETED, actualTask.getStatus());
        TASK_SERVICE.changeStatusById(user1.getId(), task.getId(), null);
        actualTask = TASK_SERVICE.findOneById(user1.getId(), task.getId());
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(Status.COMPLETED, actualTask.getStatus());
    }

    @Test
    public void testChangeTaskStatusByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.changeStatusById(null, UNKNOWN_ID, Status.COMPLETED));
    }

    @Test
    public void testBindTaskToProject() {
        final String taskId = TASK_LIST.stream().filter(p -> p.getUser().getId().equals(user1.getId())).collect(Collectors.toList()).get(4).getId();
        final int actualNumberOfEntries = TASK_SERVICE.findAllByProjectId(user1.getId(), project.getId()).size();
        final int expectedNumberOfEntries = actualNumberOfEntries + 1;
        TASK_SERVICE.bindTaskToProject(user1.getId(), taskId, project.getId());
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.findAllByProjectId(user1.getId(), project.getId()).size());
        @Nullable final Task task = TASK_SERVICE.findOneById(taskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(project.getId(), task.getProject().getId());
    }

    @Test
    public void testUnbindTaskToProject() {
        final String taskId = TASK_LIST.stream().filter(p -> p.getUser().getId().equals(user1.getId())).collect(Collectors.toList()).get(0).getId();
        final int actualNumberOfEntries = TASK_SERVICE.findAllByProjectId(user1.getId(), project.getId()).size();
        final int expectedNumberOfEntries = actualNumberOfEntries - 1;
        TASK_SERVICE.unbindTaskFromProject(user1.getId(), taskId, project.getId());
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.findAllByProjectId(user1.getId(), project.getId()).size());
        Assert.assertNull(TASK_SERVICE.findOneById(taskId).getProject());
    }

    @Test
    public void testFindAllByProjectId() {
        @NotNull final List<Task> actualTaskList = TASK_SERVICE.findAllByProjectId(user1.getId(), project.getId());
        @NotNull final List<Task> expectedTaskList = TASK_LIST
                .stream()
                .filter(p -> p.getProject() != null)
                .filter(p -> StringUtils.equals(project.getId(), p.getProject().getId()))
                .collect(Collectors.toList());
        Assert.assertEquals(expectedTaskList.size(), actualTaskList.size());
        for (int i = 0; i < expectedTaskList.size(); i++) {
            Assert.assertEquals(expectedTaskList.get(i).getId(), actualTaskList.get(i).getId());
        }
        Assert.assertEquals(Collections.emptyList(), TASK_SERVICE.findAllByProjectId(user1.getId(), null));
    }

    @Test
    public void testFindAllByProjectIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.findAllByProjectId(null, UNKNOWN_ID));
    }

    @Test
    public void testRemoveByIdForUser() {
        @NotNull final List<Task> actualUserTaskList = TASK_SERVICE.findAll(user1.getId());
        int expectedNumberOfEntries = actualUserTaskList.size();
        for (int i = 0; i < expectedNumberOfEntries; i++) {
            expectedNumberOfEntries--;
            @NotNull final Task task = actualUserTaskList.get(i);
            TASK_SERVICE.removeById(user1.getId(), task.getId());
            Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize(user1.getId()));
        }
    }

}